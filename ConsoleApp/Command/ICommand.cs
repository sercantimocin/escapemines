﻿using ConsoleApp.Models;

namespace ConsoleApp.Command
{
    public interface ICommand
    {
        void Apply(ITurtle turtle);
    }
}
