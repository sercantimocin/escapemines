﻿using ConsoleApp.Models;

namespace ConsoleApp.Command
{
    public class LeftCommand : ICommand
    {
        public void Apply(ITurtle turtle)
        {
            turtle.TurnLeft();
        }
    }
}
