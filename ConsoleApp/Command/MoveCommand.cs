﻿using ConsoleApp.Models;

namespace ConsoleApp.Command
{
    public class MoveCommand : ICommand
    {
        public void Apply(ITurtle turtle)
        {
            turtle.Move();
        }
    }
}
