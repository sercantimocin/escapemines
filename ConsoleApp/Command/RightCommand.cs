﻿using ConsoleApp.Models;

namespace ConsoleApp.Command
{
    public class RightCommand : ICommand
    {
        public void Apply(ITurtle turtle)
        {
            turtle.TurnRight();
        }
    }
}
