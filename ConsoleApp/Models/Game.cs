﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ConsoleApp.Command;

namespace ConsoleApp.Models
{
    public enum GameStatus
    {
        Success,
        MineHit,
        StillInDanger
    }

    public class Game
    {
        private Coordinate _size;
        private readonly List<Coordinate> _mines;
        private readonly Coordinate _exit;
        private readonly ITurtle _turtle;
        private GameStatus _status = GameStatus.StillInDanger;

        public Game(Coordinate size, Coordinate exit, List<Coordinate> mines, ITurtle turtle)
        {
            _size = size;
            _exit = exit;
            _mines = mines;
            _turtle = turtle;
        }

        public void ExecuteCommands(string commands)
        {
            if (string.IsNullOrEmpty(commands))
            {
                throw new ArgumentNullException(nameof(commands));
            }

            if (_status == GameStatus.StillInDanger)
            {
                foreach (var command in commands.Split(null))
                {
                    ICommand newCommand = CreateCommand(command[0]);
                    newCommand.Apply(_turtle);
                    Console.WriteLine(_turtle.GetStatus());

                    if (CheckHitMine() || CheckArriveExit())
                    {
                        return;
                    }
                }
            }
        }

        public GameStatus GetStatus()
        {
            return _status;
        }

        private ICommand CreateCommand(char commandChar)
        {
            Type type = Assembly.GetAssembly(typeof(ICommand))
                .ExportedTypes
                .FirstOrDefault(x => x.IsClass
                                     && x.BaseType != null
                                     && typeof(ICommand).IsAssignableFrom(x)
                                     && x.Name.StartsWith(char.ToUpper(commandChar)));

            if (type == null)
            {
                throw new ArgumentOutOfRangeException("Invalid command");
            }

            return (ICommand)Activator.CreateInstance(type);
        }

        private bool CheckArriveExit()
        {
            if (_exit.X == _turtle.Coordinate.X && _exit.Y == _turtle.Coordinate.Y)
            {
                _status = GameStatus.Success;
                return true;
            }

            return false;
        }

        private bool CheckHitMine()
        {
            if (_mines.Any(m => m.X == _turtle.Coordinate.X && m.Y == _turtle.Coordinate.Y))
            {
                _status = GameStatus.MineHit;
                return true;
            }

            return false;
        }
    }
}
