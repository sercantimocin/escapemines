﻿using System;
using ConsoleApp.State;

namespace ConsoleApp.Models
{
    public interface ITurtle
    {
        void Move();
        DirectionState TurnLeft();
        DirectionState TurnRight();
        string GetStatus();
        Coordinate Coordinate { get; }
    }

    public class Turtle : ITurtle
    {
        private const int Min = 0;
        private readonly int _maxX;
        private readonly int _maxY;
        private DirectionState _state;

        public Turtle(int y, int x, int maxY, int maxX, DirectionState state)
        {
            Coordinate = new Coordinate(y, x);
            _maxX = maxX;
            _maxY = maxY;
            _state = state;
        }

        public Coordinate Coordinate { get; }

        public void Move()
        {
            if (_state is North)
            {
                Coordinate.X = Math.Max(Coordinate.X - 1, Min);
            }

            if (_state is South)
            {
                Coordinate.X = Math.Min(Coordinate.X + 1, _maxX);
            }

            if (_state is West)
            {
                Coordinate.Y = Math.Max(Coordinate.Y - 1, Min);
            }

            if (_state is East)
            {
                Coordinate.Y = Math.Min(Coordinate.Y + 1, _maxY);
            }
        }

        public DirectionState TurnLeft()
        {
            _state = _state.Left();
            return _state;
        }

        public DirectionState TurnRight()
        {
            _state = _state.Right();
            return _state;
        }

        public string GetStatus()
        {
            return $"{Coordinate.X} {Coordinate.Y} {_state.GetName()}";
        }
    }
}
