﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ConsoleApp.Models;
using ConsoleApp.State;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = File.ReadAllLines(Path.Combine(Environment.CurrentDirectory, "input.txt"));

            Game game = CreateGameFromInputs(lines);

            for (int i = 4; i < lines.Length; i++)
            {
                string commands = lines[i];
                game.ExecuteCommands(commands);
            }
            Console.WriteLine(game.GetStatus());

            Console.ReadKey();
        }

        private static Game CreateGameFromInputs(string[] lines)
        {
            try
            {
                int[] boardSizes = Array.ConvertAll(lines[0].TrimEnd().Split(null), int.Parse);
                Coordinate board = new Coordinate(boardSizes[0], boardSizes[1]);

                List<Coordinate> mines = lines[1]
                    .TrimEnd()
                    .Split(null)
                    .Select(x => new Coordinate((int)char.GetNumericValue(x[0]), (int)char.GetNumericValue(x[2])))
                    .ToList();

                int[] exitPoint = Array.ConvertAll(lines[2].TrimEnd().Split(null), int.Parse);
                Coordinate exit = new Coordinate(exitPoint[0], exitPoint[1]);

                string[] turtleStartInfo = lines[3].TrimEnd().Split(null);
                Turtle turtle = new Turtle(Convert.ToInt32(turtleStartInfo[0]),
                                           Convert.ToInt32(turtleStartInfo[1]),
                                           board.Y - 1,
                                           board.X - 1,
                                           DirectionState.ConvertDirectionState(turtleStartInfo[2]));

                return new Game(board, exit, mines, turtle);
            }
            catch (Exception)
            {
                throw new ArgumentException("Invalid inputs");
            }
        }
    }
}
