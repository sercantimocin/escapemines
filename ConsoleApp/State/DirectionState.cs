﻿using System;
using System.Linq;
using System.Reflection;

namespace ConsoleApp.State
{
    public abstract class DirectionState
    {
        public abstract DirectionState Left();

        public abstract DirectionState Right();

        public string GetName()
        {
            return GetType().Name.Substring(0, 1);
        }

        public static DirectionState ConvertDirectionState(string stateChar)
        {
            Type type = Assembly.GetAssembly(typeof(DirectionState))
                                .ExportedTypes
                                .FirstOrDefault(x => x.IsClass
                                                  && x.IsSubclassOf(typeof(DirectionState))
                                                  && x.Name.StartsWith(stateChar.ToUpper()));

            if (type == null)
            {
                throw new ArgumentOutOfRangeException("Invalid state");
            }

            return (DirectionState)Activator.CreateInstance(type);
        }
    }
}
