namespace ConsoleApp.State
{
    public class East : DirectionState
    {
        public override DirectionState Left()
        {
            return new North();
        }

        public override DirectionState Right()
        {
            return new South();
        }
    }
}