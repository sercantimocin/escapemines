namespace ConsoleApp.State
{
    public class North : DirectionState
    {
        public override DirectionState Left()
        {
            return new West();
        }

        public override DirectionState Right()
        {
            return new East();
        }
    }
}