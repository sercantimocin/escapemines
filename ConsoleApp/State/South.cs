namespace ConsoleApp.State
{
    public class South : DirectionState
    {
        public override DirectionState Left()
        {
            return new East();
        }

        public override DirectionState Right()
        {
            return new West();
        }
    }
}