namespace ConsoleApp.State
{
    public class West : DirectionState
    {
        public override DirectionState Left()
        {
            return new South();
        }

        public override DirectionState Right()
        {
            return new North();
        }
    }
}