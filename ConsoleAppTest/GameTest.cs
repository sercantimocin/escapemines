using System;
using System.Collections.Generic;
using ConsoleApp.Models;
using ConsoleApp.State;
using Xunit;

namespace ConsoleAppTest
{
    public class GameTest
    {
        private Game _game;
        private Turtle _turtle;

        public GameTest()
        {
            _turtle = new Turtle(0, 1, 4, 3, new North());

            _game = new Game(new Coordinate(5, 4),
                new Coordinate(4, 2),
                new List<Coordinate>() { new Coordinate(1, 1), new Coordinate(2, 1) },
                _turtle);
        }

        [Fact]
        public void Get_Status_Default_StillInDanger()
        {
            var sut = _game.GetStatus();

            Assert.Equal(GameStatus.StillInDanger, sut);
        }

        [Fact]
        public void Move_OneStep_ToNorth_ExecuteCommands_StillInDanger()
        {
            _game.ExecuteCommands("M");

            Assert.Equal(0, _turtle.Coordinate.X);
            Assert.Equal(GameStatus.StillInDanger, _game.GetStatus());
        }

        [Fact]
        public void Move_ToExit_ExecuteCommands_Success()
        {
            _game.ExecuteCommands("R R M L M M M M");

            Assert.Equal(2, _turtle.Coordinate.X);
            Assert.Equal(4, _turtle.Coordinate.Y);
            Assert.Equal(GameStatus.Success, _game.GetStatus());
        }

        [Fact]
        public void Move_ToMine_ExecuteCommands_MineHit()
        {
            _game.ExecuteCommands("R M M M");

            Assert.Equal(1, _turtle.Coordinate.X);
            Assert.Equal(1, _turtle.Coordinate.Y);
            Assert.Equal(GameStatus.MineHit, _game.GetStatus());
        }
    }
}
