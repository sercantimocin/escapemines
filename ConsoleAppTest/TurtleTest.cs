﻿using ConsoleApp.Models;
using ConsoleApp.State;
using Xunit;

namespace ConsoleAppTest
{
    public class TurtleTest
    {
        [Fact]
        public void Initial_North_TurnLeft_Is_West()
        {
            var turtle = new Turtle(0, 0, 4, 3, new North());

            var sut = turtle.TurnLeft();

            Assert.True(sut is West);
        }

        [Fact]
        public void Initial_North_TurnRight_Is_East()
        {
            var turtle = new Turtle(0, 0, 4, 3, new North());

            var sut = turtle.TurnRight();

            Assert.True(sut is East);
        }

        [Fact]
        public void Initial_00_Coordinate_Return_00()
        {
            var turtle = new Turtle(0, 0, 4, 3, new North());

            var sut = turtle.Coordinate;

            Assert.Equal(0, sut.X);
            Assert.Equal(0, sut.Y);
        }

        [Fact]
        public void Initial_00S_Move_Then_Coordinate_01()
        {
            var turtle = new Turtle(0, 0, 4, 3, new South());

            turtle.Move();

            Assert.Equal(1, turtle.Coordinate.X);
            Assert.Equal(0, turtle.Coordinate.Y);
        }

        [Fact]
        public void Initial_00E_Move_Then_Coordinate_10()
        {
            var turtle = new Turtle(0, 0, 4, 3, new East());

            turtle.Move();

            Assert.Equal(0, turtle.Coordinate.X);
            Assert.Equal(1, turtle.Coordinate.Y);
        }

        [Fact]
        public void Initial_00W_Move_Then_Coordinate_00()
        {
            var turtle = new Turtle(0, 0, 4, 3, new West());

            turtle.Move();

            Assert.Equal(0, turtle.Coordinate.X);
            Assert.Equal(0, turtle.Coordinate.Y);
        }

        [Fact]
        public void Initial_12E_Move_Then_Coordinate_22()
        {
            var turtle = new Turtle(1, 2, 4, 3, new East());

            turtle.Move();

            Assert.Equal(2, turtle.Coordinate.X);
            Assert.Equal(2, turtle.Coordinate.Y);
        }
    }
}
